// Electron modules that we are using.
const { app, BrowserWindow, ipcMain, Tray, Menu } = require('electron');
// Modules to help with file paths and URLs.
const path = require('path');
const url = require('url');

// Variables to hold our application's main window and system tray.
let mainWindow;
let tray = null;

// Electron ipcMain listener that listens to 'get-path' requests and responds with the requested path.
ipcMain.on('get-path', (event, name) => {
  event.returnValue = app.getPath(name);
});

// Function to create the main window of our application.
function createWindow() {
  // Creating a new BrowserWindow.
  mainWindow = new BrowserWindow({
    width: 1200, // Initial window width.
    height: 600, // Initial window height.
    webPreferences: {
      nodeIntegration: true, // Enabling Node.js integration.
      contextIsolation: false // Disabling context isolation.
    },
    icon: path.join(__dirname, 'icon.png'), // The application's icon.
    autoHideMenuBar: true, // Automatically hide the menu bar.
    show: false // The window will be hidden initially.
  });

  // Loading the index.html file into our mainWindow.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Event to handle the window close event.
  mainWindow.on('close', function (event) {
    if (!app.isQuiting) {
      event.preventDefault();
      mainWindow.hide();
    }
    return false;
  });

  // Event to handle when the window is closed.
  mainWindow.on('closed', function () {
    mainWindow = null;
  });

  // Event to handle window minimize event.
  mainWindow.on('minimize', function (event) {
    event.preventDefault();
    mainWindow.hide();
  });

  // Creating a context menu for our tray icon.
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show App', click: function () {
        mainWindow.show(); // Show the main window when 'Show App' is clicked.
      }
    },
    {
      label: 'Quit', click: function () {
        app.isQuiting = true; // Set app.isQuiting to true.
        app.quit(); // Quit the application when 'Quit' is clicked.
      }
    }
  ]);

  // Creating a system tray icon.
  tray = new Tray(path.join(__dirname, 'icon.png'));
  tray.setToolTip('QuantumNotes'); // Tooltip text to be displayed when the mouse hovers over the tray icon.
  tray.setContextMenu(contextMenu); // Setting the tray icon's context menu.

  // Event to handle when the tray icon is clicked.
  tray.on('click', () => {
    mainWindow.show(); // Show the main window.
  });
}

// Event to handle when Electron has finished initialization and is ready to create browser windows.
app.on('ready', createWindow);

// Event to handle when all windows are closed.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit(); // Quit the application if not on macOS.
  }
});

// Event to handle when the application is activated.
app.on('activate', function () {
  if (mainWindow === null) {
    createWindow(); // Recreate the mainWindow.
  }
});

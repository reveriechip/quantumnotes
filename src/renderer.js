// Import required modules
const fs = require('fs-extra'); // Used for file operations
const path = require('path'); // Used for handling and transforming file paths
const xml2js = require('xml2js'); // Used for XML parsing
const { ipcRenderer } = require('electron'); // Used to communicate with the main process

// Get the userData path from the main process and construct the path for notes file
const appUserDataPath = ipcRenderer.sendSync('get-path', 'userData');
const notesFilePath = path.join(appUserDataPath, 'notes.xml');
const tabsFilePath = path.join(appUserDataPath, 'tabs.xml');

// Get HTML elements
const notesDiv = document.getElementById('notes');
const noteForm = document.getElementById('noteForm');
const noteInput = document.getElementById('noteInput');

// Initialize variables
let currentNote = null;
let offsetX = 0;
let offsetY = 0;

const backgroundColors = [{
    mainColor: '#ffffff',
    secondaryColor: '#000000'
},
{
    mainColor: '#000000',
    secondaryColor: '#ffffff'
}];

// Ensure the directory for notes file exists
fs.ensureDir(path.dirname(notesFilePath)).catch(err => console.error(err));

// Function to display notes
function displayNotes(currentTab) {
    // Clear the inner HTML
    notesDiv.innerHTML = '';



    // Read and parse the notes file
    fs.readFile(notesFilePath, (err, data) => {
        if (err) throw err;
        xml2js.parseString(data, (err, result) => {
            if (err) throw err;

            // Initialize and adjust notes
            let notes = result.notes.note || [];
            if (!Array.isArray(notes)) {
                notes = [notes];
            }

            // Calculate grid spacing for note display
            const gridSpacing = 75;
            let gridX = 0;
            let gridY = 0;
            const notesPerRow = Math.floor(window.innerWidth / (200 + gridSpacing));

            // Loop through each note
            for (const note of notes) {
                if (currentTab !== 'All' && note.$.category !== currentTab) continue;
                // Create a new div element for each note
                const noteDiv = document.createElement('div');
                noteDiv.addEventListener('dblclick', function () {
                    const input = document.createElement('input');
                    input.type = 'text';
                    input.value = note._;
                    input.addEventListener('blur', function () {
                        note._ = input.value;
                        const updatedTab = currentTab === 'All' ? note.$.category : currentTab;
                        saveNote(note, note.$?.id, updatedTab);
                        displayNotes(currentTab);
                    });
                    input.addEventListener('keydown', function (event) {
                        if (event.key === 'Enter') {
                            note._ = input.value;
                            const updatedTab = currentTab === 'All' ? note.$.category : currentTab;
                            saveNote(note, note.$?.id, updatedTab);
                            displayNotes(currentTab);
                        }
                    });
                    noteDiv.textContent = '';
                    noteDiv.appendChild(input);
                    input.focus();
                });
                noteDiv.className = 'note';
                noteDiv.textContent = note._;
                noteDiv.dataset.id = note.$?.id;

                // // Set the position for each note
                // noteDiv.style.left = `${gridX * (190 + gridSpacing)}px`;
                // noteDiv.style.top = `${gridY * (gridSpacing * 1.25)}px`;

                // Create a delete button for each note
                const deleteButton = document.createElement('button');
                deleteButton.textContent = 'X';
                deleteButton.className = 'delete-note';
                deleteButton.addEventListener('click', function () {
                    deleteNote(note.$?.id);
                });

                // Append the note and delete button to the note div, and the note div to the notes div
                noteDiv.appendChild(deleteButton);
                notesDiv.appendChild(noteDiv);

                // Increment and adjust grid position
                gridX++;
                if (gridX >= notesPerRow) {
                    gridX = 0;
                    gridY++;
                }
            }
        });
    });
}

// Function to delete a note
function deleteNote(noteId) {
    // Read and parse the notes file
    fs.readFile(notesFilePath, (err, data) => {
        if (err || !data) return;

        xml2js.parseString(data, (err, result) => {
            if (err) throw err;

            // Initialize and adjust notes
            let notes = result.notes && result.notes.note ? result.notes.note : [];
            if (!Array.isArray(notes)) {
                notes = [notes];
            }

            // Remove the note with the matching id
            notes = notes.filter(note => note.$?.id !== noteId);

            // Construct new xml from updated notes array and write it to the file
            const builder = new xml2js.Builder();
            const xml = builder.buildObject({ notes: { note: notes } });

            fs.writeFile(notesFilePath, xml, (err) => {
                if (err) throw err;
                displayNotes(currentTab);
            });
        });
    });
}

// Function to save a note
function saveNote(noteObject, id, currentTab) {
    fs.readFile(notesFilePath, (err, data) => {
        if (err || !data) {
            console.log(err);
            const xml = `<notes><note id="1" category="${currentTab}">${noteObject._}</note></notes>`;

            fs.writeFile(notesFilePath, xml, (err) => {
                if (err) {
                    console.log('Failed to write file: ', err);
                    throw err;
                }

                console.log('Note saved!');
                displayNotes(currentTab);
            });
        } else {
            xml2js.parseString(data, (err, result) => {
                if (err) {
                    console.log('Failed to parse XML: ', err);
                    throw err;
                }

                let notes = result.notes && result.notes.note ? result.notes.note : [];
                if (!Array.isArray(notes)) {
                    notes = [notes];
                }

                let maxId = 0;
                for (let i = 0; i < notes.length; i++) {
                    if (parseInt(notes[i].$.id) > maxId) {
                        maxId = parseInt(notes[i].$.id);
                    }
                }

                // Check if the id is defined. If it is, it means that the note is being updated.
                if (id) {
                    for (let i = 0; i < notes.length; i++) {
                        if (notes[i].$.id === id.toString()) {
                            notes[i]._ = noteObject._; // Update the note content.
                            notes[i].$.category = currentTab; // Update the note category.
                            break;
                        }
                    }
                    console.log('Adding new note' + noteObject._);
                } else {
                    // If id is not defined, it means that a new note is being added.
                    noteObject.$ = { id: (maxId + 1).toString(), category: currentTab };
                    notes.push(noteObject);
                }

                // Construct new xml from updated notes array and write it to the file.
                const builder = new xml2js.Builder();
                const xml = builder.buildObject({ notes: { note: notes } });

                fs.writeFile(notesFilePath, xml, (err) => {
                    if (err) throw err;
                    displayNotes(currentTab);
                });
            });
        }
    });
}

// Function to load tabs
function loadTabs() {
    fs.readFile(tabsFilePath, (err, data) => {
        if (err) {
            console.log(err);
            return;
        }

        xml2js.parseString(data, (err, result) => {
            if (err) throw err;

            // Get the tabs
            const tabsArray = result.tabs.tab;

            // Loop through each tab
            for (const tabName of tabsArray) {
                // If it's not the "All" tab, create a new div for it
                if (tabName !== 'All') {
                    createTab(tabName);
                } else {
                    // If it is the "All" tab, set the current tab to it
                    currentTab = tabName;
                    // Add the selected class to the "All" tab
                    document.querySelector(`[data-category="${tabName}"]`).classList.add('selected');
                }
            }
        });
    });
}

function saveTabs() {
    // Get all tab elements
    const tabs = tabsDiv.querySelectorAll('.tab');

    // Map the elements to their text content
    const tabsArray = Array.from(tabs).map(tab => tab.textContent);

    // Construct new XML from the tabs array and write it to the file
    const builder = new xml2js.Builder();
    const xml = builder.buildObject({ tabs: { tab: tabsArray } });

    fs.writeFile(tabsFilePath, xml, (err) => {
        if (err) throw err;
    });
}

function createTab(tabName) {
    const newTab = document.createElement('div');
    newTab.textContent = tabName;
    newTab.className = 'tab';
    newTab.dataset.category = tabName;
    tabsDiv.insertBefore(newTab, addTabButton);
}

// Event listener for note form submission
noteForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const note = noteInput.value.trim();
    const currentTab = tabsDiv.querySelector('.tab.selected').dataset.category;
    if (note) {
        saveNote({ _: note }, undefined, currentTab);
        noteInput.value = '';
    }
});

const addTabButton = document.getElementById('addTab');
addTabButton.addEventListener('click', function () {
    // Create a new input element
    const input = document.createElement('input');
    input.type = 'text';
    input.className = 'tab-create';

    // Handle when the input loses focus
    input.addEventListener('blur', function () {
        const newTabName = input.value.trim();

        if (newTabName) {
            createTab(newTabName);
            input.remove();  // Remove the input element
            saveTabs();  // Save the tabs
        } else {
            input.remove();  // Remove the input element
        }
    });

    // Insert the input element before the addTabButton
    tabsDiv.insertBefore(input, addTabButton);

    // Automatically focus on the input element
    input.focus();
});

const tabsDiv = document.getElementById('tabs');
tabsDiv.addEventListener('click', function (event) {
    if (event.target.classList.contains('tab')) {
        const selectedTab = tabsDiv.querySelector('.tab.selected');
        if (selectedTab) {
            selectedTab.classList.remove('selected');
        }
        event.target.classList.add('selected');
        displayNotes(event.target.dataset.category);
    }
});

// Display notes on load
loadTabs();
displayNotes('All');


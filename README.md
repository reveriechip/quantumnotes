# Quantum Notes

Quantum Notes is a simple, Electron-based desktop application for note taking. The application allows users to add, delete, and move notes around on the screen. The application minimizes to the system tray when not in use, providing a quick and easy access to your notes at any time.

## Features

- Add and delete notes.
- Move notes around on the screen.
- Minimize the application to the system tray.
- Refresh the notes view.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed the latest version of [Node.js and npm](https://nodejs.org/en/download/).

### Installing Quantum Notes

To install Quantum Notes, follow these steps:

```shell
git clone <repository-url>
cd <repository-directory>
npm install
```
### Creating an executable

To create an executable file, follow these steps:

```shell
npm run make
```
Navigate to /out/ directory to find quantumnotes.exe